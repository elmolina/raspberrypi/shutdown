#!/usr/bin/env python3
import datetime
import configparser
import os
import json
import subprocess
import myjdapi
import transmissionrpc

def report(action, type, element, detail):
    payload = {}
    payload['id'] = "shutdown"
    payload['date'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') 
    payload['action'] = action
    payload['type'] = type
    payload['element'] = element
    payload['detail'] = detail          
    return json.dumps(payload)   

def isTVON(macTV):
    isTVON = True
    # Call the arp command to lookup for IP TV from MAC
    output = subprocess.check_output("/usr/bin/sudo /usr/sbin/arp | grep \"" + macTV +"\" | awk ' { print $1 }'", shell=True)
    ipTV = output.decode('ascii').strip()
    if ipTV == None or not ipTV:
        isTVON = False
    else:        
        output = subprocess.check_output("/usr/bin/sudo /bin/ping -c 1 " + ipTV + " | grep \", 0% packet loss,\" | wc -l" , shell=True)        
        pingResult = output.decode('ascii').strip()

        if pingResult == None or not pingResult or pingResult == '0':
            isTVON = False
    return isTVON

def isTransmissionON(transmission_ip, transmission_port, transmission_user, transmission_password):
    isTransmissionON = False
    transmission = transmissionrpc.Client(transmission_ip, transmission_port, user = transmission_user, password = transmission_password)
    torrents = transmission.get_torrents()
    for torrent in torrents:
        if torrent.status == 'downloading':
            isTransmissionON = True
            break
    
    return isTransmissionON

def isJDownloaderON(device, username, password):
    #First of all you have to make an instance of the Myjdapi class and set your APPKey:
    jd=myjdapi.Myjdapi()
    jd.set_app_key(device)              

    """
    After that you can connect.
    Now you can only connect using username and password.
    This is a problem because you can't remember the session between executions
    for this reason i will add a way to "connect" which is actually not connecting, 
    but adding the old tokens you saved. This way you can use this between executions
    as long as your tokens are still valid without saving the username and password.
    """

    jd.connect(username, password)

    # When connecting it gets the devices also, so you can use them but if you want to 
    # gather the devices available in my.jdownloader later you can do it like this

    jd.update_devices()

    # Now you are ready to do actions with devices. To use a device you get it like this:
    device=jd.get_device(device) 

    # After that you can use the different API functions.
    # For example i want to get the packages of the downloads list, the API has a function under downloads called queryPackages,
    # you can use it with this library like this:
    downloads = device.downloads.query_packages([{
                    "enabled" : True,
                    "finished" : True,
                    "status" : True,
                    "maxResults" : -1,
                    "startAt" : 0,
                }])

    isJDownloaderON = False

    for item in downloads:
        if "enabled" in item:
            enabled = item.get("enabled")
            if enabled:
                if "finished" in item:
                    finished = item.get("finished")
                    if not finished:
                        isJDownloaderON = True
                        break
                else:
                    isJDownloaderON = True
                    break
    
    return isJDownloaderON

#MAIN
config = configparser.ConfigParser()
config.read("shutdown.ini")                         # read config file and store its values in config_parser variable 

active = config.get("Status", "active") 

macTV = config.get("TV", "mac")

startTime = config.get("Shutdown Time", "startTime")
endTime = config.get("Shutdown Time", "endTime")

now = datetime.datetime.now().time() 
start = datetime.datetime.strptime(startTime, "%H:%M").time()
end = datetime.datetime.strptime(endTime, "%H:%M").time()

credentials = configparser.ConfigParser()
credentials.read("credentials.ini")           

username = credentials.get("JDownloader", "username")
password = credentials.get("JDownloader", "password")

transmission_ip = config.get("Transmission", "ip")
transmission_port = config.get("Transmission", "port")
transmission_user = credentials.get("Transmission", "user")
transmission_password = credentials.get("Transmission", "password")

if active == '1':
    if start <= now <= end:  
        # If we are in shutdown time
        if not isTVON(macTV):
            if not isTransmissionON(transmission_ip, transmission_port, transmission_user, transmission_password):
                if not isJDownloaderON("raspberry", username, password):
                    print (report("warn", "shutdown", "RaspBerry", "Shutting Down"))
                    os.system("/usr/bin/sudo /sbin/shutdown now -h")
    else:
        print (report("info", "shutdown", "RaspBerry", "Not on Time"))
